/**
 * Creates a frame and draws a circle and a line
 */

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;

  public class DrawPanel extends JPanel{

      public DrawPanel () {
    setPreferredSize(new Dimension(500, 500));
      }

      public void paintComponent(Graphics page) {
    super.paintComponent(page);
    
    page.setColor(Color.black);
    page.fillOval(50, 75, 100, 100);
    page.drawLine(0,0, 100, 100);
      }

  }
